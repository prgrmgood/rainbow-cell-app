//
//  ViewController.swift
//  ColorTableView
//
//  Created by Noah on 9/21/15.
//  Copyright © 2015 Noah. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    weak var color : Color!
    @IBOutlet weak var colorLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        colorLabel.text = color.name
        self.view.backgroundColor = color.rgba
        
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

