//
//  ColorCell.swift
//  ColorTableView
//
//  Created by Noah on 9/21/15.
//  Copyright © 2015 Noah. All rights reserved.
//

import UIKit

class ColorCell: UITableViewCell {

    var color : Color!
    
    @IBOutlet var colorLabel: UILabel!
    @IBOutlet var colorBox: UIView!
    
 
    func setupCell(color:Color) {
        self.color = color
        colorLabel.text = color.name
        colorBox.backgroundColor = color.rgba
        
    }
    
}
